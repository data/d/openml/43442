# OpenML dataset: Is-this-a-good-customer

https://www.openml.org/d/43442

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Imbalanced classes put accuracy out of business. This is a surprisingly common problem in machine learning (specifically in classification), occurring in datasets with a disproportionate ratio of observations in each class.
Content
Standard accuracy no longer reliably measures performance, which makes model training much trickier.
Imbalanced classes appear in many domains, including:

Antifraud
Antispam


Inspiration
5 tactics for handling imbalanced classes in machine learning:

Up-sample the minority class
Down-sample the majority class
Change your performance metric
Penalize algorithms (cost-sensitive training)
Use tree-based algorithms

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43442) of an [OpenML dataset](https://www.openml.org/d/43442). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43442/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43442/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43442/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

